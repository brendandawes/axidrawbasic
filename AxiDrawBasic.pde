/*
Basic Axidraw and Processing demo based on original code from Aaron Koblin
https://github.com/koblin/AxiDrawProcessing

Additional methods by Brendan Dawes http://brendandawes.com

Needs the Node.js CNCServer available from https://github.com/techninja/cncserver 

This demo uses a direct 1:1 relationship between pixels and millimetres. 

This is rough but it's a starting point!
*/



CNCServer cnc;
boolean readyToDraw = false;
final float SCALER = 1.38;
final float XYSCALER = 3.0;


void setup() {

  cnc = new CNCServer("http://localhost:4242");
  cnc.penUp();
  println("Plotter is at home? Press 'u' to unlock, 'z' to zero, 'd' to draw");
}

void draw(){
  
  if(readyToDraw){
    axiEllipse(20,20,20,20);
    axiEllipse(20,20,10,10); 
    axiRect(20,20,40,40,true); 
    axiRect(20,20,30,30,true); 
    axiRect(20,20,20,20,true);
    readyToDraw = false;
  }
}


void axiLine(float startX, float startY, float endX, float endY){
  cnc.penUp();
  cnc.moveTo(startX/XYSCALER,(startY/XYSCALER)*SCALER);
  cnc.penDown();
  cnc.moveTo(endX/XYSCALER,(endY/XYSCALER)*SCALER);
  cnc.penUp();
}

void axiRect(float x, float y, float w,float h, boolean center){

   cnc.penUp();
   float xPos = x/XYSCALER;
   float yPos = (y/XYSCALER)*SCALER;
   if (center){
      xPos -= ((w/2.0)/XYSCALER);
      yPos -= ((h/2.0)/XYSCALER)*SCALER;
   }
   cnc.moveTo(xPos,yPos);
   cnc.penDown();
   cnc.moveTo(xPos+(w/XYSCALER),yPos);
   cnc.moveTo(xPos+(w/XYSCALER),yPos+((h/XYSCALER)*SCALER));
   cnc.moveTo(xPos,yPos+((h/XYSCALER)*SCALER));
   cnc.moveTo(xPos,yPos);
   cnc.penUp();


}

void axiEllipse(float x, float y, float w, float h){
    float offsetX = x /XYSCALER;
    float offsetY = (y/XYSCALER)*SCALER;
    int amount = 100;
    float inc = TWO_PI/amount;
    float radiusX = w/XYSCALER;
    float radiusY = (h/XYSCALER)*SCALER;
    float a = 0;
    float yMult = SCALER;
    float xPos;
    float yPos;
    xPos = offsetX+(float)sin(a)*radiusX;
    yPos = offsetY+(float)cos(a)*radiusY;
    cnc.penUp();
    cnc.moveTo(xPos,yPos);
    cnc.penDown();
    for (int i=0; i < amount+1; i++ ){
      xPos = offsetX+(float)sin(a)*radiusX;
      yPos = offsetY+(float)cos(a)*radiusY;
      cnc.moveTo(xPos,yPos);
      a += inc;
    }
    cnc.penUp();

}

void exit(){
  cnc.penUp();
  println("Goodbye!");
  super.exit();
}

void stop(){
 super.exit(); 
}

void keyPressed(){
  if(key == 'q') cnc.penUp();
  if(key == 'a') cnc.penDown();
  if(key == 'u'){
    cnc.unlock();
    println("Pen unlocked ..... remember to zero!");
  }
  if(key == 'z'){
    cnc.zero();
    println("Pen zero'd");
  }
  if(key == 'd'){
     cnc.penDown();
     readyToDraw = true;
     println("Drawing");
  }
}