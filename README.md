AxiDraw and Processing Example
==============================

Basic Axidraw and Processing demo based on original code from Aaron Koblin
https://github.com/koblin/AxiDrawProcessing

Additional methods by Brendan Dawes http://brendandawes.com

Needs the Node.js CNCServer available from https://github.com/techninja/cncserver 

This demo uses a direct 1:1 relationship between pixels and millimetres. 

More details on AxiDraw at http://axidraw.com

This is rough but it's a starting point :)